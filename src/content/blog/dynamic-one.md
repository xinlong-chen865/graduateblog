---
author: Lucas
pubDatetime: 2023-01-25T08:51:33.000Z
title: 不要被“动态规划”的名字吓住 —— 初识篇（一）
postSlug: dynamic-one
featured: false
ogImage: http://120.48.20.245:4000/static/upload/07e6ed6d47cb49be07befdf4e3f57e09.jpeg
tags:
  - algorithm
description: 动态规划的算法思想其实并不难，听起来要规划什么的，好像跟数学有关系，其实半毛钱关系都没有。动态规划思想是算法中最简单的思想，也是最暴力的算法，用一句话简单概括，把计算过的结果保存下来，如果下次再用就直接取存起来的结果。
---

## 一、动态规划不会跟数学有关系吧？

动态规划的算法思想其实并不难，听起来要规划什么的，好像跟数学有关系，其实半毛钱关系都没有。动态规划思想是算法中最简单的思想，也是最暴力的算法，用一句话简单概括，把计算过的结果保存下来，如果下次再用就直接取存起来的结果。那么，通过优化斐波那契函数来初识动态规划思想，最好不过了

## 二、斐波那契数列

写一个函数，输入 n ，求 斐波那契（Fibonacci）数列 的第 n 项（即 F(N)）。斐波那契数列的定义如下：

F(0) = 0

F(1) = 1

F(N) = F(N - 1) + F(N - 2)

其中 N > 1. 斐波那契数列由 0 和 1 开始，之后的斐波那契数就是由之前的两数相加而得出。

```
function main(num) {
    const fib = (n) => {
        if (n < 2) return n;
        return fib(n - 1) + fib(n - 2);
    }

    return fib(num);
}

// 执行
main(5);
```

我们通过递归很简单地实现了斐波那契数列
![斐波那契数列](http://www.lucasfe.cn:4000/static/upload/f7c74049e2e56418e53b2413c95890dd.png)

通过这个递归执行的图片，我们很容易的发现一个问题，这样直接递归效率肯定不高，因为 Fib(2) 被计算了两次，如果我们把它事先存起来，那么再计算的时候直接取值，就不用继续递归下去了，效率能大大的提高。在执行的过程中，进行一些缓存优化，这种思想就是动态规划（动态指的就是执行中，规划就是做的一些策略）

```
function main(num) {
    const cache = {};
    const fib = (n) => {
        if (n < 2) return n;
        if (typeof cache[n] !== 'undefined') return cache[n];

        cache[n] = fib(n - 1) + fib(n - 2);
        return cache[n] ;
    }

    return fib(num);
}

// 执行
main(5);
```

![execute](http://www.lucasfe.cn:4000/static/upload/0a01f80ec3111becd4235e52b0b90e00.png)

在传入 num=40 的时候，优化的代码执行效率提高了千倍，随着 num 的增大，效率提升还会更多

## 三、走迷宫问题

小明置身于一个迷宫，请你帮小明找出从起点到终点有多少种走法。
小明只能向下和向右移动。

![迷宫](http://www.lucasfe.cn:4000/static/upload/b694142e051dab727be4ed580e2b65f3.png)

这道题也是动态规划中很经典的一道题目。首先我们用递归的思想来分析一下，右下角出口的走法，是等于它左边和上面的走法加在一起

所以我们得出了 dp 公式（下一篇文章我会详细解释 dp 公式）：**_dp[x][y] = dp[x-1][y] + dp[x][y-1]_**

```
const dp = [];
for (let index = 0; index < 100; index++) {
    dp[index] = new Array(100).fill(undefined)
}
dp[0][1] = 1;
dp[1][0] = 1;

function main(x, y) {
    if (x < 0 || y < 0) {
        return 0;
    }
    if (dp[x][y]) {
        return dp[x][y];
    }
    const res = dp[x][y] = main(x - 1, y) + main(x, y -1)
    return res;
}

main(2, 2);
```

我来解释一下这段代码，首先一般 dp 的题目，都会有一个 init 的过程，0-5 行就是 init。main 函数中的两个条件语句，保证了不会走出界，和计算过就不计算了

![计算过程](http://www.lucasfe.cn:4000/static/upload/ea98ca8b3688a8411e509bc325578fc7.png)

我建议，刷算法题，主要还是看代码，学习算法地思路。如果想不通，可以像我一样画一个图，慢慢理解。毕竟好记性不如烂笔头嘛哈哈哈

## 四、小结

这篇文章通过递归的方式，渐进式地带着各位初识了动态规划思想，但是动态规划的大部分写法，不是这种由后往前的递归思想，而是从前往后的递推的思想（递推思想更适合 dp 公式地思考），下一篇文章，我将通过几个题目，带各位更细致的了解这种思想
