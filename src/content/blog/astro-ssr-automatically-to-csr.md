---
author: Lucas
pubDatetime: 2023-11-18T12:51:33.000Z
title: Astro框架SSR自动降级CSR
postSlug: astro-ssr-automatically-to-csr
featured: true
ogImage: http://120.48.20.245:4000/static/upload/07e6ed6d47cb49be07befdf4e3f57e09.jpeg
tags:
  - frontend
description: Astro 作为最近爆火的前端框架，其优势和适用场景在这里就不过多赘述了，本文着重介绍一下我在使用过程中发现的一个可以在特定场景下将SSR自动降级为CSR的方法。
---

文章摘要：Astro 作为一个最近爆火的前端框架，可以实现 SSR 或构建 SSG 资源，我们可以通过一些方式实现渲染模式之间的优雅降级。

# 前言

Astro 作为最近爆火的前端框架，其优势和适用场景在这里就不过多赘述了，本文着重介绍一下我在使用过程中发现的一个可以在特定场景下将 SSR 自动降级为 CSR 的方法。

# 为什么要做自动降级

当我们使用 Astro 框架构建 SSR 服务时，可以选择控制适配器是构建为 middleware 模式还是 standalone 模式。

虽然 standalone 模式可以非常轻松快速的实现服务的部署，但实际使用中，因为监控和稳定性等各方面的考虑，更多人可能会选择 middleware 模式作为自建 nodejs 项目的中间件来进行部署。

虽然 SSR 可以在大多数场景下显著提升页面的加载性能，但毕竟多了一层服务端渲染的处理，其稳定性肯定是不如传统的静态资源服务的，其中一个很不稳定的点就是当服务端渲染异常时该如何处理。

不管你是使用 middleware 模式还是 standalone 模式，Astro 当遇见这种情况的时候都会直接向客户端返回 500，这对客户端的体验是非常差的，会导致服务的可用性大大下降。
所以，有没有一种办法能当 SSR 失败的时候自动切换成 CSR，直接向客户端返回 SSG 资源呢？

# 解决思路

首先看下 Astro 对 SSR 错误是如何进行处理的：

在 node_modules/@astrojs/node/dist/nodeMiddleware.js 文件下的 nodeMiddleware_default 方法，能找到这样一段代码：

```
try {
    const route = app.match(req);
    if (route) {
        try {
            const response = await route.handler(req, route, locals);
            await writeWebResponse(app, res, response);
        } catch (err) {
            if (next) {
                next(err);
            } else {
                throw err;
            }
        }
    } else if (next) {
        return next();
    } else {
        const response = await app.render(req);
        await writeWebResponse(app, res, response);
    }
} catch (err) {
    if (!res.headersSent) {
        res.writeHead(500, `Server error`);
        res.end();
    }
}

```

可以看到，Astro 在这里对 SSR 过程中的异常进行 catch 后，直接进行了返回，致使页面报错 Server error。

因为 Astro 擅自对 SSR 的错误进行处理，导致我们在使用 middleware 模式时无法对错误进行收集和处理。

_所以我们是不是可以让 Astro 不对 SSR 错误进行处理，直接抛出异常，然后当 nodejs 在接收到 SSR 异常时自动切换到 CSR 呢。_

# 解决方法

Nodejs 以使用 Koa2 框架为例：

1.通过 plugin 改写刚刚那段代码，让 Astro 将错误正常抛出。

```
try {
    const route = app.match(req);
    if (route) {
        const response = await route.handler(req, route, locals);
        await writeWebResponse(app, res, response);
    } else if (next) {
        return next();
    } else {
        const response = await app.render(req);
        await writeWebResponse(app, res, response);
    }
} catch (err) {
    next();
    throw err;
}
```

2.写一个 nodejs 错误收集中间件，让程序在接到请求过程中的错误时可以正常运行。

```
app.use(async (ctx, next) => {
    try {
        await next();
    } catch (err) {
        console.log(err);
    }
})
```

3.更改打包流水线，同时打出 SSR 和 SSG 两种模式的包。

4.在 SSR 中间件后注册静态资源托管，将 SSG 的资源进行托管。

```
app.use(c2k(ssrHandler)); // 这是Astro的中间件
app.use(koaStatic(path.join(process.cwd(), './dist/ssg/client')));
```

```
import Koa from 'koa';
import koaStatic from 'koa-static';
import c2k from 'koa-connect';
import { handler as ssrHandler } from '../dist/ssr/server/entry.mjs';
import path from 'path';
import healthRouter from './middleware/router.mjs';
import logger, { getCookieInfoByCtx } from './logger.mjs';

const app = new Koa();

app.use(async (ctx, next) => {
    try {
        await next();
    } catch (err) {
        console.log('middlewareError', err);
    }
});

app.use(healthRouter.routes());
app.use(c2k(ssrHandler));
app.use(koaStatic(path.join(process.cwd(), './dist/ssg/client')));

app.on('error', (err, ctx) => {
    console.log('onerror', err);
});

process.on('unhandledRejection', (err) => {
    console.log('unhandledRejection', err);
});

const port = +process.env.port || 3000;
process.stdout.write(`[${new Date().toLocaleString()}] starting at ${port}.`);
app.listen(port);
```

这样就实现了一个简单的从 SSR 到 CSR 的自动降级。

# 总结

这个从 SSR 自动降级为 CSR 的方案在理论上是能提升一些用户体验的，并且结合一些错误收集和监控的方案，也能让我们更快的对 SSR 过程中产生的错误进行感知和处理，大大提升项目的稳定性和可用性。

这个方案也有其缺点，例如：写代码的时候要兼顾 SSR 和 CSR 两种模式、暂时没有找到比较优雅的改写 Astro 源码的方法等，使用与否就看大家如何权衡了。

最后欢迎大家对 Astro 框架的实践和落地经验一起沟通分享。
